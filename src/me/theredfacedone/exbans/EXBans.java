package me.theredfacedone.exbans;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import me.theredfacedone.exbans.cmd.CmdBan;
import me.theredfacedone.exbans.cmd.CmdBanID;
import me.theredfacedone.exbans.cmd.CmdKick;
import me.theredfacedone.exbans.cmd.CmdUnban;
import me.theredfacedone.exbans.event.PlayerLoginEventListener;
import me.theredfacedone.exbans.event.PlayerQuitEventListener;
import me.theredfacedone.lib.sql.MySQLDataManager;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class EXBans extends JavaPlugin{

	private final Logger             logger     = this.getLogger();
	private final FileConfiguration  cfg        = this.getConfig();
	private final File               cfgFile    = new File(this.getDataFolder(), "config.yml");
	private final String             cfgVersion = "0.2";
	private final ArrayList <String> whitelist  = new ArrayList <String>();
	private String                   sql_address;
	private String                   sql_username;
	private String                   sql_password;
	private MySQLDataManager         sqlm;

	@Override
	public void onEnable(){
		if (cfgFile.exists()){
			String version = cfg.getString("plugin.cfg-version");
			if (!version.equals(cfgVersion)){
				cfgFile.renameTo(new File(this.getDataFolder(), "config-" + version + ".old"));
				this.saveDefaultConfig();
			}
		}else{
			this.saveDefaultConfig();
		}
		this.saveResource("whitelist.cfg", false);
		sql_address =
		    "jdbc:mysql://" + cfg.getString("mysql.address") + ":" + cfg.getString("mysql.port") + "/"
		        + cfg.getString("mysql.db");
		sql_username = cfg.getString("mysql.username");
		sql_password = cfg.getString("mysql.password");
		sqlm = new MySQLDataManager(sql_address, sql_username, sql_password);
		try{
			if (sqlm != null){
				sqlm.openConnection();
				if (sqlm.isConnected()){
					try{
						sqlm.execUpdate(
						    "CREATE TABLE IF NOT EXISTS exbans_users(Username,UUID,BanReason,BannedBy,UnbanDate);",
						    (String) null);
					}catch (SQLException e){
						logger.log(Level.SEVERE,
						    "Unable to create the banned users table on database. Details below:\n", e);
					}
				}else{
					if (cfg.getBoolean("server.safe-mode")){
						logger.log(Level.WARNING,
						    "Unable to connect to the banned users database. Nobody will be allowed to join.");
					}else{
						logger.log(Level.WARNING, "Unable to connect to the banned users database. Anyone can join!");
					}
				}
			}

		}catch (SQLException e){
			logger.log(Level.SEVERE, "Unable to open connection to banned users database. Details below:\n", e);
		}
		if (cfg.getBoolean("server.safe-mode")){
			this.loadWhitelist();
		}
		this.getServer().getPluginManager().registerEvents(new PlayerLoginEventListener(this), this);
		this.getServer().getPluginManager().registerEvents(new PlayerQuitEventListener(), this);
		this.getCommand("ban").setExecutor(new CmdBan(this));
		this.getCommand("kick").setExecutor(new CmdKick(this));
		this.getCommand("unban").setExecutor(new CmdUnban(this));
		this.getCommand("banid").setExecutor(new CmdBanID(this));
		logger.log(Level.INFO, "EXBans v" + this.getDescription().getVersion() + " enabled.");
	}

	@Override
	public void onDisable(){
		whitelist.clear();
		try{
			sqlm.close();
		}catch (SQLException e){
			logger.log(Level.SEVERE, "Unable to open connection to banned users database. Details below:\n", e);
		}
		logger.log(Level.INFO, "EXBans v" + this.getDescription().getVersion() + " disabled.");
	}

	public MySQLDataManager getSQLM(){
		return sqlm;
	}

	// Makes coloring chat messages less of a pain in the ass.
	public static String colorize(String message){
		return message.replaceAll("&", "\u00A7");
	}

	// Because Bukkit is shit and doesn't want us getting online players by
	// name.
	public static Player getPlayer(String name){
		Player[] players = Bukkit.getOnlinePlayers();
		ArrayList <Player> candidates = new ArrayList <Player>();
		for (int i = 0; i < players.length; i++){
			if (players[i].getName().equalsIgnoreCase(name)){
				return players[i];
			}else if (players[i].getName().toLowerCase().contains(name.toLowerCase())){
				candidates.add(players[i]);
			}
		}
		// Prevent any ambiguity in names.
		if (candidates.size() > 1 || candidates.isEmpty()) return null;
		return candidates.get(0);
	}

	public static Player getPlayerByID(String id){
		Player[] players = Bukkit.getOnlinePlayers();
		for (Player p : players){
			if (id.toLowerCase().equals(p.getUniqueId().toString().toLowerCase())) return p;
		}
		return null;
	}

	// Takes the time argument from one of the ban commands and turns it into a
	// Date.
	// Say our time arg is 1d, it returns a Date 1 day in the future.
	public static Date parseTime(String arg){
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());

		if (arg.matches("(\\d+)(\\w{1})")){
			int val;

			try{
				val = Integer.parseInt(arg.substring(0, arg.length() - 2));
			}catch (NumberFormatException e){
				return null;
			}

			switch (arg.indexOf(arg.length() - 1)){
				case 'm':
					cal.add(Calendar.MINUTE, val);
					return cal.getTime();
				case 'M':
					cal.add(Calendar.MINUTE, val);
					return cal.getTime();
				case 'h':
					cal.add(Calendar.HOUR_OF_DAY, val);
					return cal.getTime();
				case 'H':
					cal.add(Calendar.HOUR_OF_DAY, val);
					return cal.getTime();
				case 'd':
					cal.add(Calendar.DAY_OF_YEAR, val);
					return cal.getTime();
				case 'D':
					cal.add(Calendar.DAY_OF_YEAR, val);
					return cal.getTime();
				default:
					return null;
			}
		}

		return null;
	}

	// Load the whitelist into memory so we don't have to read from the config
	// every time we check it.
	private void loadWhitelist(){
		File f = new File(this.getDataFolder(), "whitelist.cfg");
		BufferedReader in;
		try{
			String line;
			in = new BufferedReader(new FileReader(f));
			while ((line = in.readLine()) != null){
				if (!line.isEmpty() && !line.startsWith("#")){
					whitelist.add(line.trim()); // Add the line to the
					                            // whitelist, after trimming off
					                            // any unnecessary whitespace.
				}
			}
		}catch (FileNotFoundException e){
			logger.log(Level.SEVERE,
			    "Unable to find whitelist.cfg file. Nobody will be allowed to join if safe mode is enabled.");
			return;
		}catch (IOException e){
			logger.log(Level.SEVERE,
			    "Unable to read from whitelist.cfg file. Nobody will be allowed to join if safe mode is enabled.");
			return;
		}
		try{
			in.close();
		}catch (IOException e){
			logger.log(Level.SEVERE, "Unable to close BufferedReader for whitelist.cfg. Report this to a dev ASAP.", e);
			return;
		}
	}

	public ArrayList <String> getWhitelist(){
		return whitelist;
	}

	public static String formatBanMessage(String reason, boolean appeal, String appealLink, boolean banner,
	    String bannerName){
		StringBuilder kickMsg = new StringBuilder();
		kickMsg.append("You are banned. Reason:\n" + reason);
		if (appeal) kickMsg.append("\nAppeal this ban at " + appealLink);
		if (banner) kickMsg.append("\nYou were banned by: " + bannerName);
		return kickMsg.toString();
	}

	public static String formatKickMessage(String reason, boolean banner, String bannerName){
		StringBuilder kickMsg = new StringBuilder();
		kickMsg.append("You were kicked. Reason:\n" + reason);
		if (banner) kickMsg.append("\nYou were kicked by: " + bannerName);
		return kickMsg.toString();
	}
}

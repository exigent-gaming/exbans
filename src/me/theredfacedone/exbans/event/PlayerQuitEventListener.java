package me.theredfacedone.exbans.event;

import me.theredfacedone.exbans.EXBans;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerQuitEventListener implements Listener{

	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerQuit(PlayerQuitEvent e){
		for (Player p : Bukkit.getOnlinePlayers()){
			if (p.hasPermission("exbans.echoid")){
				p.sendMessage("&e"
				    + EXBans.colorize(e.getPlayer().getName() + " &fhas left the server. \nTheir UUID is &e"
				        + e.getPlayer().getUniqueId().toString()));
			}
		}
	}
}

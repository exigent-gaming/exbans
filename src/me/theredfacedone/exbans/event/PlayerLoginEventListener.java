package me.theredfacedone.exbans.event;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import me.theredfacedone.exbans.EXBans;
import me.theredfacedone.lib.sql.MySQLDataManager;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;

public class PlayerLoginEventListener implements Listener{

	private final EXBans           plugin;
	private final MySQLDataManager sqlm;
	private final Logger           logger;
	private final boolean          safemode;
	private final String           webLink;
	private final boolean          showAppealLink;
	private final boolean          showBanner;

	public PlayerLoginEventListener(EXBans plugin){
		this.plugin = plugin;
		this.sqlm = plugin.getSQLM();
		this.logger = plugin.getLogger();
		this.safemode = plugin.getConfig().getBoolean("server.safe-mode");
		this.webLink = plugin.getConfig().getString("server.web-link");
		this.showAppealLink = plugin.getConfig().getBoolean("server.kick-msg.appeal-link");
		this.showBanner = plugin.getConfig().getBoolean("server.kick-msg.banner");
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onLogin(PlayerLoginEvent e){
		// Tell the console if we aren't able to communicate with the MySQL
		// database.
		try{
			if (sqlm == null || !sqlm.isConnected()){
				logger.log(Level.SEVERE, "Unable to communicate with SQL database.");
				if (safemode && !plugin.getWhitelist().contains(e.getPlayer().getUniqueId().toString())){
					e.setKickMessage("Server is currently locked down and you are not whitelisted. Try again later.");
					e.setResult(Result.KICK_OTHER);
					return;
				}
			}
		}catch (SQLException ex){
			logger.log(Level.SEVERE, "Unable to communicate with SQL database.", ex);
			if (safemode && !plugin.getWhitelist().contains(e.getPlayer().getUniqueId().toString())){
				e.setKickMessage("Server is currently locked down and you are not whitelisted. Try again later.");
				e.setResult(Result.KICK_OTHER);
				return;
			}
		}
		try{
			ResultSet rs =
			    sqlm.execQuery("SELECT UUID,BanReason,BannedBy,UnbanDate FROM exbans_users WHERE UUID=?",
			        e.getPlayer().getUniqueId().toString());
			while (rs.next()){
				if (new SimpleDateFormat("MM d yy").parse(rs.getString("UnbanDate")).after(new Date())){
					e.setKickMessage(EXBans.formatBanMessage(rs.getString("BanReason"), showAppealLink, webLink,
					    showBanner, rs.getString("BannedBy")));
					e.setResult(Result.KICK_BANNED);
					return;
				}
			}
		}catch (SQLException | ParseException ex){
			e.setKickMessage("Unable to check if you are banned. Report this to a dev on the forums please.\n"
			    + webLink);
			e.setResult(Result.KICK_OTHER);
			return;
		}
		for (Player p : Bukkit.getOnlinePlayers()){
			if (p.hasPermission("exbans.echoid")){
				p.sendMessage("&e"
				    + EXBans.colorize(e.getPlayer().getName() + " &fhas joined the server. \nTheir UUID is &e"
				        + e.getPlayer().getUniqueId().toString()));
			}
		}
	}
}

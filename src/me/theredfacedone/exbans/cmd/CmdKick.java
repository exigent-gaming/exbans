package me.theredfacedone.exbans.cmd;

import me.theredfacedone.exbans.EXBans;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CmdKick implements CommandExecutor{

	private final StringBuilder reason = new StringBuilder();
	private final boolean       showBanner;

	public CmdKick(EXBans plugin){
		this.showBanner = plugin.getConfig().getBoolean("server.kick-msg.banner");
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		// If the command sender has permission to use this command...
		if (sender.hasPermission("exbans.kick")){
			// ...and has provided a sufficient amount of arguments...
			if (args.length > 1){
				// we kick them.
				String bannerName = (sender instanceof Player) ? sender.getName() : "Console";
				Player victim;
				if ((victim = EXBans.getPlayer(args[0])) == null){
					sender.sendMessage(EXBans.colorize("&cInvalid player or ambiguous name."));
					return true;
				}
				for (int i = 1; i < args.length; i++){
					reason.append(args[i]);
				}
				victim.kickPlayer(EXBans.formatKickMessage(reason.toString(), showBanner, bannerName));
				sender.sendMessage(EXBans.colorize("&aKicked player."));
				return true;
			}
			return false;
		}
		sender.sendMessage(EXBans.colorize("&cInsufficient permissions."));
		return true;
	}

}

package me.theredfacedone.exbans.cmd;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import me.theredfacedone.exbans.EXBans;
import me.theredfacedone.lib.sql.MySQLDataManager;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class CmdUnban implements CommandExecutor{

	private final Logger     logger;
	private MySQLDataManager sqlm;

	public CmdUnban(EXBans plugin){
		this.logger = plugin.getLogger();
		this.sqlm = plugin.getSQLM();
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		// Disable command if we can't communicate with MySQL database.
		try{
			if (sqlm == null || !sqlm.isConnected()){
				sender.sendMessage("No connection to a MySQL database has been established. This command is disabled.");
				return true;
			}
		}catch (SQLException e){
			logger.log(Level.SEVERE, "Unable to communicate with SQL database.", e);
			return true;
		}
		// If the command sender is allowed to use this command...
		if (sender.hasPermission("exbans.unban")){
			// ...and has provided a sufficient amount of arguments...
			if (args.length > 0){
				// we finally unban the user.
				try{
					ResultSet rs = sqlm.execQuery("SELECT * FROM exbans_users where UUID=?", args[0]);
					if (!rs.first()){
						sender.sendMessage(EXBans.colorize("&aInvalid UUID"));
						return true;
					}
					sqlm.execUpdate("INSERT INTO exbans_users(UnbanDate) VALUES(?) WHERE UUID=?;",
					    new SimpleDateFormat("mm d yy").format(new Date()), args[0]);
					sender.sendMessage(EXBans.colorize("&aUser has been unbanned."));
				}catch (SQLException e){
					logger.log(Level.SEVERE, "Unable to unban a player.", e);
					return true;
				}
			}
			return false;
		}
		sender.sendMessage(EXBans.colorize("&cInsufficient permissions."));
		return true;
	}

}

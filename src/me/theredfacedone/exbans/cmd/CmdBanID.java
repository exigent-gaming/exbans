package me.theredfacedone.exbans.cmd;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import me.theredfacedone.exbans.EXBans;
import me.theredfacedone.lib.sql.MySQLDataManager;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CmdBanID implements CommandExecutor{

	private final Logger        logger;
	private MySQLDataManager    sqlm;
	private final StringBuilder reason = new StringBuilder();
	private final String        webLink;
	private final boolean       showAppealLink;
	private final boolean       showBanner;

	public CmdBanID(EXBans plugin){
		this.logger = plugin.getLogger();
		this.sqlm = plugin.getSQLM();
		this.webLink = plugin.getConfig().getString("server.web-link");
		this.showAppealLink = plugin.getConfig().getBoolean("server.kick-msg.appeal-link");
		this.showBanner = plugin.getConfig().getBoolean("server.kick-msg.banner");
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		// Disable command if we can't communicate with MySQL database.
		try{
			if (sqlm == null || !sqlm.isConnected()){
				sender.sendMessage("No connection to a MySQL database has been established. This command is disabled.");
				return true;
			}
		}catch (SQLException e){
			logger.log(Level.SEVERE, "Unable to communicate with SQL database.", e);
			return true;
		}
		// If the command sender is allowed to use this command...
		if (sender.hasPermission("exbans.banid")){
			// ...and has provided a sufficient amount of arguments...
			if (args.length > 2){
				// we finally do the banning.

				// If the person issuing the ban is doing so from the console,
				// we don't try to get their name and cause a bunch of errors.
				String bannerName = (sender instanceof Player) ? sender.getName() : "Console";
				Player victim;
				Date unbanDate;
				if ((unbanDate = EXBans.parseTime(args[1])) == null){
					sender.sendMessage(EXBans.colorize("&cInvalid time argument."));
					return true;
				}
				// Append all other arguments to the reason.
				for (int i = 2; i < args.length; i++){
					reason.append(args[i]);
				}
				if ((victim = EXBans.getPlayerByID(args[0])) == null){
					try{
						sqlm.execUpdate("INSERT INTO exbans_users VALUES (?,?,?,?,?);", "Unknown", args[0],
						    reason.toString(), bannerName, new SimpleDateFormat("MM d yy").format(unbanDate));
						sender.sendMessage("&aUUID has been banned.");
						return true;
					}catch (SQLException e){
						sender.sendMessage("&cUnable to ban player. Check console or alert a higher staff member ASAP.");
						logger.log(Level.SEVERE, "Unable to submit ban", e);
						return true;
					}
				}
				try{
					ResultSet dupe =
					    sqlm.execQuery("SELECT * FROM exbans_users WHERE UUID=?;", victim.getUniqueId().toString());
					while (dupe.next()){
						try{
							if (new SimpleDateFormat("MM d yy").parse(dupe.getString("UnbanDate")).after(new Date())){
								// This should never happen, but you can't be
								// too careful.
								sender.sendMessage(EXBans.colorize("&aSomething has gone horribly wrong. This user should be banned, but is on the server."));
								return true;
							}
						}catch (ParseException e){
							sender.sendMessage("&cUnable to ban player. Check console or alert a higher staff member ASAP.");
							logger.log(Level.SEVERE, "Unable to parse date from MySQL database.", e);
							return true;
						}
					}
					sqlm.execUpdate("INSERT INTO exbans_users VALUES (?,?,?,?,?);", victim.getName(),
					    victim.getUniqueId().toString(), reason.toString(), bannerName,
					    new SimpleDateFormat("MM d yy").format(unbanDate));
					victim.kickPlayer(EXBans.formatBanMessage(reason.toString(), showAppealLink, webLink, showBanner,
					    bannerName));
					sender.sendMessage("&aUUID has been banned.");
					return true;
				}catch (SQLException e){
					sender.sendMessage("&cUnable to ban player. Check console or alert a higher staff member ASAP.");
					logger.log(Level.SEVERE, "Unable to submit ban", e);
					return true;
				}
			}
			return false;
		}
		sender.sendMessage(EXBans.colorize("&cInsufficient permissions."));
		return true;
	}

}
